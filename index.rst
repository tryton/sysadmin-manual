==========================================================
Tryton System Administrators Manual
==========================================================

Target audience:
System Administrators administering Tryton installations
on the operating-system level.

This manual describes how to install and configure
the Tryton server `trytond`.
It also described how to install the Tryton Web Client `sao`
and how to install the Tryton GUI client for users.


.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
